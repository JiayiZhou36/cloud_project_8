# Cloud Project 8

## Purpose of Project
This project creates a Rust command-Line tool with testing which removes leading and trailing whitespace in a string and return a formatted string.

## Requirements
* Rust command-line tool
* Data ingestion/processing-- remove leading and trailing whitespace in a string and return a formatted string
* Unit tests