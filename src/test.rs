use super::*;

#[tokio::test]
async fn test_handle_request() {
    let request = Request {
        command: "test".into(),
    };
    let req_id = "test_req_id".into();
    let response = handle_request(request, req_id).await;
    assert_eq!(response.req_id, "test_req_id");
    assert_eq!(response.msg, "Command test.");
}