use serde::{Deserialize, Serialize};

/// This is a made-up example. Requests come into the runtime as unicode
/// strings in json format, which can map to any structure that implements `serde::Deserialize`
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize)]
pub struct Request {
    pub command: String,
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into json. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize)]
pub struct Response {
    pub req_id: String,
    pub msg: String,
}

/// Function handler logic
pub async fn handle_request(request: Request, req_id: String) -> Response {
    // Extract some useful info from the request
    let command = request.command.trim();

    // Prepare the response
    Response {
        req_id,
        msg: format!("Command {}.", command),
    }
}