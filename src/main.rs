use std::env;
use structopt::StructOpt;
use tokio;

mod lambda_handler;

#[derive(Debug, StructOpt)]
#[structopt(name = "command-line-tool")]
struct Opt {
    #[structopt(short, long)]
    command: String,
}

#[tokio::main]
async fn main() {
    let opt = Opt::from_args();
    let req_id = env::var("REQ_ID").unwrap_or_else(|_| "default".into()); // Default req_id for CLI
    let request = lambda_handler::Request {
        command: opt.command,
    };
    let response = lambda_handler::handle_request(request, req_id).await;
    println!("{}", serde_json::to_string(&response).unwrap());
}
